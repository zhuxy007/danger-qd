import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_FORMULA_DETAIL',
        create: 'DATA_FORMULA_CREATE',
        update: 'DATA_FORMULA_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [
        {
          prop: 'name',
          default: '',
          label: '公式名称',
          rule: { required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.name } clearable/>
        },
        {
          prop: 'content',
          default: '',
          label: '公式内容',
          rule: {required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.content } clearable/>
        },
        {
          prop: 'model_id',
          default: '',
          rule: {required: true, message: '必填', trigger: 'change'},
          label: '所属模型',
          render: () => <d2-dict-select name="model_sta" vModel={ this.form.model.model_id } style="width: 100%;" custom stringify/>
        }
      ]
    }
  }
}
