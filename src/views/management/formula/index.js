import utils from '@/utils'
import table from '@/mixins/crud-table.js'

export default {
  mixins: [ table ],
  components: {
    componentForm: () => import('./form')
  },
  render () {
    const page =
      <d2-container spacious>
        <d2-search-panel slot="header" vModel={ this.search.panel.active }>
          <d2-bar slot="title">
            <d2-bar-space/>
            { this.p('query') ? <d2-bar-cell>{ this.vNodePaginationMini }</d2-bar-cell> : <d2-bar-cell>{ this.vNodeSearchPanelAlertNoPermissionQuery }</d2-bar-cell> }
            <d2-bar-space/>
            <d2-bar-cell>
              <el-button-group>
                { this.p('query') ? this.vNodeButtonSearch : null }
                { this.vNodeButtonTableColumnsFilterTrigger }
              </el-button-group>
            </d2-bar-cell>
            { this.p('add') ? <d2-bar-cell>{ this.vNodeButtonCreate }</d2-bar-cell> : null }
          </d2-bar>
          { this.p('query') ? this.vNodeSearchForm : null }
        </d2-search-panel>
        { this.vNodeTable }
        <d2-bar slot="footer">
          <d2-bar-cell>{ this.vNodePaginationFull }</d2-bar-cell>
          <d2-bar-space/>
        </d2-bar>
        <component-form ref="form" on-success={ this.research }/>
        { this.vNodeTableColumnsFilter }
      </d2-container>
    return page
  },
  data () {
    return {
      api: {
        index: 'DATA_FORMULA_ALL',
        delete: 'DATA_FORMULA_DELETE'
      },
      permission: {
        query: 'system:formula:query',
        add: 'system:formula:add',
        edit: 'system:formula:edit',
        remove: 'system:formula:remove'
      }
    }
  },
  methods: {
    loadDict () {
      // 实验数据
      this.loadDictOne({
        name: 'model_sta',
        method: this.$api.DATA_MODEL_ALL,
        fields: 'id,name',
        path: 'list',
        label: 'name'
      })
    }
  },
  computed: {
    // 配置项
    // 表格列
    // 建议的书写顺序 [prop] -> [label] -> [align] -> [minWidth][width] -> [fixed] -> [other] -> [render][formatter] -> [if][show]
    settingColumns () {
      return [
        { prop: 'name', label: '公式名称', align: 'center', minWidth: '50px', headerAlign: 'center', sortable: false },
        { prop: 'content', label: '公式内容', align: 'center', minWidth: '50px' },
        { prop: 'model_id', label: '所属模型', align: 'center', minWidth: '50px', render: ({ row }) => <d2-dict name="model_sta" value={ row.model_id } custom/> },
        { prop: 'create_by', label: '创建人员', align: 'center', width: '100px', show: false },
        { prop: 'created_at', label: '创建时间', align: 'center', width: '200px', formatter: row => utils.time.format(row.created_at, 'YYYY/M/D HH:mm:ss'), show: false },
        { prop: 'update_by', label: '更新人员', align: 'center', width: '100px', show: false },
        { prop: 'updated_at', label: '更新时间', align: 'center', width: '200px', formatter: row => utils.time.format(row.updated_at, 'YYYY/M/D HH:mm:ss'), show: false }
      ].map(setting => {
        // setting.sortable = 'custom'
        return setting
      })
    },
    // 配置项
    // 表格操作列配置
    settingActionsConfig () {
      return ({ row }) => [
        ...this.p('edit', [{ icon: 'el-icon-edit-outline', action: () => this.edit(row.id) }], []),
        ...this.p('remove', [{ icon: 'el-icon-delete', type: 'danger', confirm: `确定删除 [ ${row.name} ] 吗`, action: () => this.delete(row.id) }], [])
      ]
    },
    // 配置项
    // 表格搜索条件
    // 建议的书写顺序 [prop] -> [label] -> [default] -> [render] -> [if][show]
    settingSearch () {
      return [
        {
          prop: 'name',
          label: '公式名称',
          default: '',
          render: () => <el-input vModel={ this.search.form.model.name } style="width:120px;margin-left:20px;" clearable/>
        }
      ]
    }
  }
}
