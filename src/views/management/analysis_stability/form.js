import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_ANALYSIS_DETAIL',
        create: 'DATA_ANALYSIS_CREATE',
        update: 'DATA_ANALYSIS_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [
        {
          prop: 'name',
          default: '',
          label: '实验分析名称',
          rule: { required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.name } clearable/>
        },
        {
          prop: 'user_name',
          default: '',
          label: '实验分析人员',
          rule: {required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.user_name } clearable/>
        },
        {
          prop: 'test_time',
          default: '',
            label: '实验时间',
            rule: {required: true, message: '必填', trigger: 'change'},
            render: () =>  <el-date-picker vModel={ this.form.model.test_time } value-format="yyyy-MM-dd HH:mm:ss" type="datetime" placeholder="" style="width:200px;"/>
        },
        {
          prop: 'model_id',
          default: '',
            label: '模型',
            render: () => <d2-dict-select name="model_sta" vModel={ this.form.model.model_id } style="width: 100%;" custom stringify/>
        },
        {
          prop: 'data_id',
          default: '',
          label: '实验数据',
            render: () => <d2-dict-select name="data_sta" vModel={ this.form.model.data_id } style="width: 100%;" custom stringify multiple/>
        },
      {
        prop: 'amount',
      default: '',
          label: '装填量',
          rule: {required: true, message: '必填', trigger: 'change'},
        render: () => <el-input vModel={ this.form.model.amount } clearable/>
      },
      {
        prop: 'tempRate',
      default: '',
          label: '升温速率',
          rule: {required: true, message: '必填', trigger: 'change'},
        render: () => <el-input vModel={ this.form.model.tempRate } clearable/>
      },
        {
          prop: 'type',
              label: '类型',
        default: 1,
            show: false
        }

      ]
    }
  }
}
