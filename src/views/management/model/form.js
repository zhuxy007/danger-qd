import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_MODEL_DETAIL',
        create: 'DATA_MODEL_CREATE',
        update: 'DATA_MODEL_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [
        {
          prop: 'name',
          default: '',
          label: '模型名称',
          rule: { required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.name } clearable/>
        },
        {
          prop: 'type',
          default: this.$env.VUE_APP_DICT_EMPTY_NUMBER,
          label: '模型类型',
          col: { span: 24 },
          rule: { required: true, message: '必填', trigger: 'change'},
          render: () => <d2-dict-select name="model_type" vModel={ this.form.model.type }  button/>
        }
      ]
    }
  }
}
