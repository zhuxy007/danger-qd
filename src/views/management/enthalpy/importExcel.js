import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_ENTHALPY_DETAIL',
        create: 'DATA_ENTHALPY_CREATE',
        update: 'DATA_ENTHALPY_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [

        {
          prop: 'url',
          default: '',
            label: '反应物数据',
            render: () => <d2-file-uploader vModel={ this.form.model.url } path="template/反应物模板.xls" />
        }
      ]
    }
  }
}
