import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_ENTHALPY_DETAIL',
        create: 'DATA_ENTHALPY_CREATE',
        update: 'DATA_ENTHALPY_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [
        {
          prop: 'name',
          default: '',
          label: '名称',
          rule: { required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.name } clearable/>
        },
        {
          prop: 'hvalue',
          default: '',
          label: '数值',
          rule: {required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.hvalue } clearable/>
        },
        {
          prop: 'type',
        default: this.$env.VUE_APP_DICT_STATUS_ACTIVE,
            label: '方法',
            col: { span: 8 },
          render: () => <d2-dict-radio vModel={ this.form.model.type }  style="width:250px;" name="assess_method" button/>
        }
      ]
    }
  }
}
