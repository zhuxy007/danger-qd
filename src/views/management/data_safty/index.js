import utils from '@/utils'
import table from '@/mixins/crud-table.js'

export default {
  mixins: [ table ],
  components: {
    componentForm: () => import('./form')
  },
  render () {
    const page =
      <d2-container spacious>
        <d2-search-panel slot="header" vModel={ this.search.panel.active }>
          <d2-bar slot="title">
            <d2-bar-space/>
            { this.p('query') ? <d2-bar-cell>{ this.vNodePaginationMini }</d2-bar-cell> : <d2-bar-cell>{ this.vNodeSearchPanelAlertNoPermissionQuery }</d2-bar-cell> }
            <d2-bar-space/>
            <d2-bar-cell>
              <el-button-group>
                { this.p('query') ? this.vNodeButtonSearch : null }
                { this.vNodeButtonTableColumnsFilterTrigger }
              </el-button-group>
            </d2-bar-cell>
            { this.p('add') ? <d2-bar-cell>{ this.vNodeButtonCreate }</d2-bar-cell> : null }
          </d2-bar>
          { this.p('query') ? this.vNodeSearchForm : null }
        </d2-search-panel>
        { this.vNodeTable }
        <d2-bar slot="footer">
          <d2-bar-cell>{ this.vNodePaginationFull }</d2-bar-cell>
          <d2-bar-space/>
        </d2-bar>
        <component-form ref="form" on-success={ this.research }/>
        { this.vNodeTableColumnsFilter }
      </d2-container>
    return page
  },
  data () {
    return {
      api: {
        index: 'DATA_STABILITY_ALL',
        delete: 'DATA_STABILITY_DELETE'
      },
      permission: {
        query: 'system:data_stability:query',
        add: 'system:data_stability:add',
        edit: 'system:data_stability:edit',
        remove: 'system:data_stability:remove'
      }
    }
  },
  computed: {
    // 配置项
    // 表格列
    // 建议的书写顺序 [prop] -> [label] -> [align] -> [minWidth][width] -> [fixed] -> [other] -> [render][formatter] -> [if][show]
    settingColumns () {
      return [
        { prop: 'name', label: '实验数据名称', align: 'center', minWidth: '50px', headerAlign: 'center', sortable: false },
        { prop: 'user_name', label: '实验人员', align: 'center', minWidth: '50px' },
        { prop: 'test_room', label: '实验室', align: 'center', minWidth: '50px' },
        { prop: 'device_name', label: '实验设备', align: 'center', minWidth: '50px' },
        { prop: 'test_time', label: '实验时间', align: 'center', minWidth: '100px', formatter: row => utils.time.format(row.test_time, 'YYYY/M/D HH:mm:ss') },
        { prop: 'create_by', label: '创建人员', align: 'center', width: '100px', show: false },
        { prop: 'created_at', label: '创建时间', align: 'center', width: '200px', formatter: row => utils.time.format(row.created_at, 'YYYY/M/D HH:mm:ss'), show: false },
        { prop: 'update_by', label: '更新人员', align: 'center', width: '100px', show: false },
        { prop: 'updated_at', label: '更新时间', align: 'center', width: '200px', formatter: row => utils.time.format(row.updated_at, 'YYYY/M/D HH:mm:ss'), show: false }
      ].map(setting => {
        // setting.sortable = 'custom'
        return setting
      })
    },
    // 配置项
    // 表格操作列配置
    settingActionsConfig () {
      return ({ row }) => [
        ...this.p('edit', [{ icon: 'el-icon-edit-outline', action: () => this.edit(row.id) }], []),
        ...this.p('remove', [{ icon: 'el-icon-delete', type: 'danger', confirm: `确定删除 [ ${row.name} ] 吗`, action: () => this.delete(row.id) }], [])
      ]
    },
    // 配置项
    // 表格搜索条件
    // 建议的书写顺序 [prop] -> [label] -> [default] -> [render] -> [if][show]
    settingSearch () {
      return [
        {
          prop: 'name',
          label: '实验数据名称',
          default: '',
          render: () => <el-input vModel={ this.search.form.model.name } style="width:120px;margin-left:20px;" clearable/>
        },
        {
          prop: 'user_name',
          label: '实验人员',
          default: '',
          render: () => <el-input vModel={ this.search.form.model.user_name } style="width:120px;margin-left:20px;" clearable/>
        },
        {
          prop: 'device_name',
          label: '实验设备',
          default: '',
          render: () => <el-input vModel={ this.search.form.model.device_name } style="width:120px;margin-left:20px;" clearable/>
        },
          {
            prop: 'type',
                label: '类型',
          default: 2,
              show: false
          }
      ]
    }
  }
}
