import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_STABILITY_DETAIL',
        create: 'DATA_STABILITY_CREATE',
        update: 'DATA_STABILITY_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [
        {
          prop: 'name',
          default: '',
          label: '实验数据名称',
          rule: { required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.name } clearable/>
        },
        {
          prop: 'user_name',
          default: '',
          label: '实验人员',
          rule: {required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.user_name } clearable/>
        },
        {
          prop: 'test_time',
          default: '',
            label: '实验时间',
            rule: {required: true, message: '必填', trigger: 'change'},
            render: () =>  <el-date-picker vModel={ this.form.model.test_time } value-format="yyyy-MM-dd HH:mm:ss" type="datetime" placeholder="" style="width:200px;"/>
        },
        {
          prop: 'test_room',
          default: '',
          label: '实验室',
          render: () => <el-input vModel={ this.form.model.test_room } clearable/>
        },
        {
          prop: 'device_name',
          default: '',
          label: '实验设备',
          render: () => <el-input vModel={ this.form.model.device_name } clearable/>
        },
        {
          prop: 'url',
          default: '',
          label: '实验数据',
          render: () => <d2-file-uploader vModel={ this.form.model.url } path="template/安全泄放数据模板.xls" />
        },
        {
          prop: 'type',
              label: '类型',
        default: 2,
            show: false
        }
      ]
    }
  }
}
