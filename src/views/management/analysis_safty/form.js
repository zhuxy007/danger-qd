import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_ANALYSIS_DETAIL',
        create: 'DATA_ANALYSIS_CREATE',
        update: 'DATA_ANALYSIS_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [
        {
          prop: 'name',
          default: '',
          label: '实验分析名称',
          rule: { required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.name } clearable/>
        },
        {
          prop: 'user_name',
          default: '',
          label: '实验分析人员',
          rule: {required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.user_name } clearable/>
        },
        {
          prop: 'test_time',
          default: '',
            label: '实验时间',
            rule: {required: true, message: '必填', trigger: 'change'},
            render: () =>  <el-date-picker vModel={ this.form.model.test_time } value-format="yyyy-MM-dd HH:mm:ss" type="datetime" placeholder="" style="width:200px;"/>
        },
        {
          prop: 'data1_id',
          default: '',
          label: '闭口数据',
            render: () => <d2-dict-select name="safty_data" vModel={ this.form.model.data1_id } style="width: 100%;" custom stringify/>
        },
      {
        prop: 'data2_id',
      default: '',
          label: '开口数据',
          render: () => <d2-dict-select name="safty_data" vModel={ this.form.model.data2_id } style="width: 100%;" custom stringify/>
      },
      {
        prop: 'data3_id',
      default: '',
          label: '开口背压数据',
          render: () => <d2-dict-select name="safty_data" vModel={ this.form.model.data3_id } style="width: 100%;" custom stringify/>
      },
        {
          prop: 'type',
              label: '类型',
        default: 2,
            show: false
        }

      ]
    }
  }
}
