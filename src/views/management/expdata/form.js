import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_EXPDATA_DETAIL',
        create: 'DATA_EXPDATA_CREATE',
        update: 'DATA_EXPDATA_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [
        {
          prop: 'name',
          default: '',
          label: '名称',
          rule: { required: true, message: '必填', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.name } clearable/>
        },
        {
          prop: 'lfl',
          default: '',
          label: '爆炸下限',
          rule: {required: false, message: '', trigger: 'change'},
          render: () => <el-input vModel={ this.form.model.lfl } clearable/>
        },

      {
        prop: 'loc',
      default: '',
          label: '极限氧含量',
          rule: {required: false, message: '', trigger: 'change'},
        render: () => <el-input vModel={ this.form.model.loc } clearable/>
      },
      {
        prop: 'llft',
      default: '',
          label: '下限火焰温度',
          rule: {required: false, message: '', trigger: 'change'},
        render: () => <el-input vModel={ this.form.model.llft } clearable/>
      }, {
        prop: 'tmax',
      default: '',
            label: '最大火焰温度',
            rule: {required: false, message: '', trigger: 'change'},
        render: () => <el-input vModel={ this.form.model.tmax } clearable/>
      },
      {
        prop: 'su',
      default: '',
          label: '基本燃烧速率',
          rule: {required: false, message: '', trigger: 'change'},
        render: () => <el-input vModel={ this.form.model.su } clearable/>
      },
      {
        prop: 'lmie',
      default: '',
          label: '最小点火能',
          rule: {required: false, message: '', trigger: 'change'},
        render: () => <el-input vModel={ this.form.model.lmie } clearable/>
      },
      {
        prop: 'qd',
      default: '',
          label: '淬息距离',
          rule: {required: false, message: '', trigger: 'change'},
        render: () => <el-input vModel={ this.form.model.qd } clearable/>
      }
      ]
    }
  }
}
