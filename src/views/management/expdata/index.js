import utils from '@/utils'
import table from '@/mixins/crud-table.js'
import importExcel from "@/views/management/enthalpy/importExcel";

export default {
  mixins: [ table ],
  components: {
    componentForm: () => import('./form'),
    componentImportExcel: () => import('./importExcel'),
    componentAnalysis: () => import('./analysisTab')
  },
  render () {
    const page =
    <d2-container spacious>
          <d2-search-panel slot="header" vModel={ this.search.panel.active } v-show={this.showList}>
              <d2-bar slot="title">
              <d2-bar-space/>
              { this.p('query') ? <d2-bar-cell>{ this.vNodePaginationMini }</d2-bar-cell> : <d2-bar-cell>{ this.vNodeSearchPanelAlertNoPermissionQuery }</d2-bar-cell> }
            <d2-bar-space/>
              <d2-bar-cell >
              <el-button-group>
              { this.p('query') ? this.vNodeButtonSearch : null }
              { this.vNodeButtonTableColumnsFilterTrigger }
            </el-button-group>
              <el-button-group>
              <d2-button
                icon="el-icon-search"
                label="导入"
                type="primary"
                style="margin-left:20px"
                on-click={ () => this.importExcel() }
                thin/>
                </el-button-group>
              </d2-bar-cell>
              { this.p('add') ? <d2-bar-cell>{ this.vNodeButtonCreate }</d2-bar-cell> : null }
              </d2-bar>
                { this.p('query') ? this.vNodeSearchForm : null }
          </d2-search-panel>
            { this.vNodeTable }
          <d2-bar slot="footer" v-show={this.showList}>
              <d2-bar-cell>{ this.vNodePaginationFull }</d2-bar-cell>
              <d2-bar-space/>
           </d2-bar>
          <component-form ref="form" on-success={ this.research }/>
          <component-importExcel ref="importExcel" on-success={ this.research }/>
          { this.vNodeTableColumnsFilter }

    </d2-container>
    return page
  },
  data () {
    return {
      api: {
        index: 'DATA_EXPDATA_ALL',
        delete: 'DATA_EXPDATA_DELETE'
      },
      permission: {
        query: 'system:analysis:query',
        add: 'system:analysis:add',
        edit: 'system:analysis:edit',
        remove: 'system:analysis:remove'
      }
    }
  },
  methods: {
    loadDict () {

    }
  },
  computed: {
    // 配置项
    // 表格列
    // 建议的书写顺序 [prop] -> [label] -> [align] -> [minWidth][width] -> [fixed] -> [other] -> [render][formatter] -> [if][show]
    settingColumns () {
      return [
        { prop: 'name', label: '名称', align: 'center', minWidth: '50px', headerAlign: 'center', sortable: false },
        { prop: 'lfl', label: '爆炸下限', align: 'center', minWidth: '50px' },
        { prop: 'loc', label: '极限氧含量', align: 'center', minWidth: '50px' },
        { prop: 'llft', label: '下限火焰温度', align: 'center', minWidth: '50px' },
        { prop: 'tmax', label: '最大火焰温度', align: 'center', minWidth: '50px' },
        { prop: 'su', label: '基本燃烧速率', align: 'center', minWidth: '50px' },
        { prop: 'lmie', label: '最小点火能', align: 'center', minWidth: '50px' },
        { prop: 'qd', label: '淬息距离', align: 'center', minWidth: '50px' }
      ].map(setting => {
        // setting.sortable = 'custom'
        return setting
      })
    },
    // 配置项
    // 表格操作列配置
    settingActionsConfig () {
      return ({ row }) => [
        ...this.p('edit', [{ icon: 'el-icon-edit-outline', action: () => this.edit(row.id) }], []),
        ...this.p('remove', [{ icon: 'el-icon-delete', type: 'danger', confirm: `确定删除 [ ${row.name} ] 吗`, action: () => this.delete(row.id) }], [])
      ]
    },
    // 配置项
    // 表格搜索条件
    // 建议的书写顺序 [prop] -> [label] -> [default] -> [render] -> [if][show]
    settingSearch () {
      return [
        {
          prop: 'name',
          label: '名称',
          default: '',
          render: () => <el-input vModel={ this.search.form.model.name } style="width:120px;margin-left:20px;" clearable/>
        }
      ]
    }
  }
}
