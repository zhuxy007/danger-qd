import form from '@/mixins/crud-form'

export default {
  mixins: [ form ],
  data () {
    return {
      api: {
        detail: 'DATA_EXPDATA_DETAIL',
        create: 'DATA_EXPDATA_CREATE',
        update: 'DATA_EXPDATA_UPDATE'
      }
    }
  },
  computed: {
    setting () {
      return [

        {
          prop: 'url',
          default: '',
            label: '燃爆数据',
            render: () => <d2-file-uploader vModel={ this.form.model.url } path="template/燃爆数据.xls" />
        }
      ]
    }
  }
}
