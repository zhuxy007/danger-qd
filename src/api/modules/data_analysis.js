export default ({ request }) => ({
  /**
   * @description 岗位列表
   * @description http://yapi.xiya.vip/project/11/interface/api/75
   */
  DATA_ANALYSIS_ALL (query = {}) {
    return request({
      url: '/api/analysis/index',
      method: 'post',
      data: query
    })
  },
  /**
   * @description 岗位创建
   * @description http://yapi.xiya.vip/project/11/interface/api/80
   */
  DATA_ANALYSIS_CREATE (data) {
    return request({
      url: '/api/analysis/create',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位详情
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_ANALYSIS_DETAIL (id) {
    return request({
      url: '/api/analysis/update',
      method: 'post',
      data: {
        id
      }
    })
  },
  /**
   * @description 岗位编辑
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_ANALYSIS_UPDATE (data) {
    return request({
      url: '/api/analysis/update',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位删除
   * @description http://yapi.xiya.vip/project/11/interface/api/90
   */
  DATA_ANALYSIS_DELETE (id) {
    return request({
      url: '/api/analysis/delete',
      method: 'delete',
      data: {
        id
      }
    })
  },
  DATA_ANALYSIS_STATS () {
    return request({
      url: '/api/analysis/stats',
      method: 'get'
    })
  },
  DATA_ANALYSIS_DATASTATS () {
    return request({
      url: '/api/analysis/dataStats',
      method: 'get'
    })
  },
  DATA_ANALYSIS_GET () {
    return request({
      url: '/api/analysis/getAnalysis',
      method: 'get'
    })
  }
})
