export default ({ request }) => ({
  /**
   * @description 岗位列表
   * @description http://yapi.xiya.vip/project/11/interface/api/75
   */
  DATA_FORMULA_ALL (query = {}) {
    return request({
      url: '/api/formula/index',
      method: 'post',
      data: query
    })
  },
  /**
   * @description 岗位创建
   * @description http://yapi.xiya.vip/project/11/interface/api/80
   */
  DATA_FORMULA_CREATE (data) {
    return request({
      url: '/api/formula/create',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位详情
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_FORMULA_DETAIL (id) {
    return request({
      url: '/api/formula/update',
      method: 'post',
      data: {
        id
      }
    })
  },
  /**
   * @description 岗位编辑
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_FORMULA_UPDATE (data) {
    return request({
      url: '/api/formula/update',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位删除
   * @description http://yapi.xiya.vip/project/11/interface/api/90
   */
  DATA_FORMULA_DELETE (id) {
    return request({
      url: '/api/formula/delete',
      method: 'delete',
      data: {
        id
      }
    })
  }
})
