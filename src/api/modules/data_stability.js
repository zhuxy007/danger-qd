export default ({ request }) => ({
  /**
   * @description 岗位列表
   * @description http://yapi.xiya.vip/project/11/interface/api/75
   */
  DATA_STABILITY_ALL (query = {}) {
    return request({
      url: '/api/data_stability/index',
      method: 'post',
      data: query
    })
  },
  /**
   * @description 岗位创建
   * @description http://yapi.xiya.vip/project/11/interface/api/80
   */
  DATA_STABILITY_CREATE (data) {
    return request({
      url: '/api/data_stability/create',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位详情
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_STABILITY_DETAIL (id) {
    return request({
      url: '/api/data_stability/update',
      method: 'post',
      data: {
        id:id
      }
    })
  },
  DATA_STABILITY_DATA (pid,model) {
    return request({
      url: '/api/data_stability/getData',
      method: 'post',
      data: {
        pid:pid,
        model:model
      }
    })
  },
  DATA_STABILITY_ADJUST (pid,heat,start,window,order,startAdjust,endAdjust) {
    return request({
      url: '/api/data_stability/adjust',
      method: 'post',
      data: {
        pid:pid,
        heat:heat,
        start:start,
        window:window,
        order:order,
        startAdjust:startAdjust,
        endAdjust:endAdjust
      }
    })
  },
  DATA_STABILITY_CUT (pid,heat,start) {
    return request({
      url: '/api/data_stability/cut',
      method: 'post',
      data: {
        pid:pid,
        heat:heat,
        start:start
      }
    })
  },
  DATA_STABILITY_NLevel (pid, start, end,  heat,tp,method) {
    return request({
      url: '/api/data_stability/stabilityNlevel',
      method: 'post',
      data: {
        pid: pid,
        start: start,
        end: end,
        heat: heat,
        t:tp,
        method: method
      }
    })
  },
  DATA_STABILITY_SELFCATALYSIS (pid, start, end,  heat,tp) {
    return request({
      url: '/api/data_stability/selfCatalysis',
      method: 'post',
      data: {
        pid: pid,
        start: start,
        end: end,
        heat: heat,
        t:tp
      }
    })
  },
  DATA_STABILITY_SAMERATEONE (pid, start, end,  heat,tp) {
    return request({
      url: '/api/data_stability/sameRateOne',
      method: 'post',
      data: {
        pid: pid,
        start: start,
        end: end,
        heat: heat,
        t:tp
      }
    })
  },
  DATA_STABILITY_SAMERATEALL (pid, start, end,  heat,tp,huo,method) {
    return request({
      url: '/api/data_stability/sameRateAll',
      method: 'post',
      data: {
        pid: pid,
        start: start,
        end: end,
        heat: heat,
        t:tp,
        huo:huo,
        method:method
      }
    })
  },
  DATA_STABILITY_INSULATOR (pid, start,startTemp,endTemp,heat,window,order,startAdjust,endAdjust) {
    return request({
      url: '/api/data_stability/insulatorAnalysis',
      method: 'post',
      data: {
        pid: pid,
        start: start,
        startTemp:startTemp,
        endTemp:endTemp,
        heat: heat,
        window:window,
        order:order,
        startAdjust:startAdjust,
        endAdjust:endAdjust
      }
    })
  },

  DATA_STABILITY_FIT (xx, yy,tp) {
    return request({
      url: '/api/data_stability/insulatorFit',
      method: 'post',
      data: {
        xx: xx,
        yy: yy,
        tp: tp
      }
    })
  },

  DATA_STABILITY_MULTIFIT (dataList) {
    return request({
      url: '/api/data_stability/insulatorMultiFit',
      method: 'post',
      data: {
        dataList: dataList
      }
    })
  },
  DATA_STABILITY_NEW_FIT (pid, start,heat,startTemp, endTemp) {
    return request({
      url: '/api/data_stability/insulatorNewFit',
      method: 'post',
      data: {
        pid: pid,
        start: start,
        heat: heat,
        startTemp:startTemp,
        endTemp:endTemp
      }
    })
  },
  /**
   * @description 岗位编辑
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_STABILITY_UPDATE (data) {
    return request({
      url: '/api/data_stability/update',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位删除
   * @description http://yapi.xiya.vip/project/11/interface/api/90
   */
  DATA_STABILITY_DELETE (id) {
    return request({
      url: '/api/data_stability/delete',
      method: 'delete',
      data: {
        id
      }
    })
  },
  DATA_SAFTY_DATA (pid) {
    return request({
      url: '/api/data_stability/getSaftyData',
      method: 'post',
      data: {
        pid:pid
      }
    })
  },
  DATA_SAFTY_ADJUST (pid,window,order) {
    return request({
      url: '/api/data_stability/adjustSafty',
      method: 'post',
      data: {
        pid:pid,
        window:window,
        order:order
      }
    })
  },
  DATA_DEAL_DATA (pid,initStart,initEnd,data2Id,data3Id,window,order) {
    return request({
      url: '/api/data_stability/dealData',
      method: 'post',
      data: {
        pid:pid,
        initStart:initStart,
        initEnd:initEnd,
        data2Id:data2Id,
        data3Id:data3Id,
        window:window,
        order:order
      }
    })
  },
  DATA_COMPUTE_PARAM (pid,initStart,initEnd,data2Id,pr,pe,window,order) {
    return request({
      url: '/api/data_stability/computeParam',
      method: 'post',
      data: {
        pid:pid,
        initStart:initStart,
        initEnd:initEnd,
        data2Id:data2Id,
        pr:pr,
        pe:pe,
        window:window,
        order:order
      }
    })
  },
  DATA_COMPUTE_OMEGA (w) {
    return request({
      url: '/api/data_stability/computenc',
      method: 'post',
      data: {
        w:w
      }
    })
  },
  DATA_ENTHALPY_DATA (type) {
    return request({
      url: '/api/data_stability/getEnthalpyData',
      method: 'post',
      data: {
        type:type
      }
    })
  },
  DATA_SAVE_DATA (id,pic,startTemp,hCount,tdtf) {
    return request({
      url: '/api/data_stability/saveResult',
      method: 'post',
      data: {
        id:id,
        pic:pic,
        startTemp:startTemp,
        hCount:hCount,
        tdtf:tdtf,
      }
    })
  },
})
