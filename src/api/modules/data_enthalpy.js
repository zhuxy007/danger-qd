export default ({ request }) => ({
  /**
   * @description 岗位列表
   * @description http://yapi.xiya.vip/project/11/interface/api/75
   */
  DATA_ENTHALPY_ALL (query = {}) {
    return request({
      url: '/api/enthalpy/index',
      method: 'post',
      data: query
    })
  },
  /**
   * @description 岗位创建
   * @description http://yapi.xiya.vip/project/11/interface/api/80
   */
  DATA_ENTHALPY_CREATE (data) {
    return request({
      url: '/api/enthalpy/create',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位详情
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_ENTHALPY_DETAIL (id) {
    return request({
      url: '/api/enthalpy/update',
      method: 'post',
      data: {
        id
      }
    })
  },
  /**
   * @description 岗位编辑
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_ENTHALPY_UPDATE (data) {
    return request({
      url: '/api/enthalpy/update',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位删除
   * @description http://yapi.xiya.vip/project/11/interface/api/90
   */
  DATA_ENTHALPY_DELETE (id) {
    return request({
      url: '/api/enthalpy/delete',
      method: 'delete',
      data: {
        id
      }
    })
  }
})
