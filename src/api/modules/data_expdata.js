export default ({ request }) => ({
  /**
   * @description 岗位列表
   * @description http://yapi.xiya.vip/project/11/interface/api/75
   */
  DATA_EXPDATA_ALL (query = {}) {
    return request({
      url: '/api/expdata/index',
      method: 'post',
      data: query
    })
  },
  /**
   * @description 岗位创建
   * @description http://yapi.xiya.vip/project/11/interface/api/80
   */
  DATA_EXPDATA_CREATE (data) {
    return request({
      url: '/api/expdata/create',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位详情
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_EXPDATA_DETAIL (id) {
    return request({
      url: '/api/expdata/update',
      method: 'post',
      data: {
        id
      }
    })
  },
  /**
   * @description 岗位编辑
   * @description http://yapi.xiya.vip/project/11/interface/api/95
   */
  DATA_EXPDATA_UPDATE (data) {
    return request({
      url: '/api/expdata/update',
      method: 'put',
      data
    })
  },
  /**
   * @description 岗位删除
   * @description http://yapi.xiya.vip/project/11/interface/api/90
   */
  DATA_EXPDATA_DELETE (id) {
    return request({
      url: '/api/expdata/delete',
      method: 'delete',
      data: {
        id
      }
    })
  }
})
